# Install Device-Simulator package
Sirve para tener un simulador de dispositivos moviles como en android studio
- asegurate de que en package manager en la pestaña advanced este colocado "show previous packages"
- window -> package manager -> Device simulator -> install
# Unity Remote 5
Sirve para testear el prjecto desde tu movil
- debes instalar las dependencias de Android en file -> building settings
- instalados tienes que ir a la carpeta SDK-platform_tools de la dependencias de android (la ruta empieza desde la carpeta de usuario ) y dar permisos de ejecucion a el archivo adb
# Touch
Los touch tienen phases para describir si esta siendo tocado o si ya no hay nadie tocando la pantalla

# LAST SPRINT
## Implementar varias pantallas
- nueva escena -> una torre medieval
- menu de navegacion de escenas

## Añadir boton salir
añadir el boton de salir al menu principal

## Añadir vida
el jugador tiene una cantidad de vida que disminuye al ser atacado y la cantidad depende del arma usada

## Añadir sonido
- sonido de fondo
- 3 efectos de sonido ( lanzar arma, chocar con jugador, chocar con suelo)

## Añadir systema de particulas
Al golpear al jugador que se vea una chispa por el metal de su armadura 

