﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boomerang : MonoBehaviour
{
    private float atractionForce = 1;
    private Vector2 initPos;
    private int maxVelocity = 1;
    private int velocity = 20;
    private Vector3 touchPosition;
    private Vector2 velToTouch;
    // message from GameManager
    void OnCreate(Vector3 touchPosition)
    {
        this.touchPosition = touchPosition;
        GetComponent<AudioSource>().Play();
    }
    // Start is called before the first frame update
    void Start()
    {
        initPos = transform.position;
        float distance = (touchPosition - transform.position).magnitude;
        velToTouch = (touchPosition - transform.position).normalized * distance * 0.05f;
    }

    // Update is called once per frame
    void Update()
    {
        if (touchPosition != Vector3.zero) move();
    }

    void move()
    {
        Vector2 steeringVector = (Vector3) initPos - transform.position;
        // fuerza de cambio de direccion
        Vector2 steeringForce = steeringVector.normalized * atractionForce * Time.deltaTime;
        // sumar la fuerza de cambio de direccion a la velocidad y sumarlo a la posicion
        velToTouch += steeringForce;
        if (velToTouch.magnitude > maxVelocity)
            velToTouch = initPos.normalized * maxVelocity * Time.deltaTime;

        //transform.position += (Vector3)velToTouch;
        GetComponent<Rigidbody2D>().MovePosition(transform.position + (Vector3) velToTouch);
    }
}
