﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aim : MonoBehaviour
{
    private Vector2 steering;
    public float steeringForce = 500;
    private Vector3 touchPosition;
    // message from GameManager
    void OnCreate(Vector3 touchPosition)
    {
        this.touchPosition = touchPosition;
        float distance = (touchPosition - transform.position).magnitude ;
        steering = ((Vector2)touchPosition - GetComponent<Rigidbody2D>().position).normalized * steeringForce * distance * 0.3f;
        GetComponent<Rigidbody2D>().AddForce(steering);
        GetComponent<AudioSource>().Play();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // touch screen and fire the ball ( commmented: could be useful after)
        //if(Input.touchCount > 0) {
        //    Touch touch = Input.GetTouch(0);
        //    if(touch.phase == TouchPhase.Began){
        //    touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
        //    touchPosition.z = 0f;
        //    steering = ((Vector2)touchPosition - GetComponent<Rigidbody2D>().position).normalized * steeringForce;
        //    GetComponent<Rigidbody2D>().AddForce(steering);
        //    }
        //}
    }
}
