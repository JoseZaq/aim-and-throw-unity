﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private Vector3 touchPosition;
    public GameObject weapon;
    public GameObject gameOverPanel;
    private bool run;

    // Start is called before the first frame update
    void Start() { }

    // Update is called once per frame
    void Update()
    {
        if (run)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                {
                    touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                    touchPosition.z = 0f;
                    createWeapon();
                }
            }
        }
    }

    /*fire the weapon*/
    void createWeapon()
    {
        Vector3 initPosition = new Vector3(-11, 0.12f, 0);
        GameObject _weapon = Instantiate(weapon, initPosition, Quaternion.identity);
        _weapon.SendMessage("OnCreate", touchPosition);
    }

    /*srt the weapon to use*/
    public void setWeapon(GameObject weapon)
    {
        this.weapon = weapon;
    }
    /* run the game*/
    public void startGame()
    {
        run = true;
    }

    public void gameOver()
    {
        run = false;
        gameOverPanel.SetActive(true);
    }

    /*Reset the Scene of the game*/
    public void resetGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /*Change Scene*/
    public void changeScene(string sceneName)
    {
        /*SceneManager.GetAllScenes()*/
        SceneManager.LoadScene(sceneName);
    }

    /*close game*/
    public void quitGame()
    {       
        /*Application.Quit();*/
        UnityEditor.EditorApplication.isPlaying = false;
    }
}
