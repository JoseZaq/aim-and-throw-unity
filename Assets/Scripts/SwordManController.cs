﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordManController : MonoBehaviour
{
    // vars
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if( other.gameObject.tag == "weapon") {
            GetComponent<Animator>().SetBool("isDead",true);
            GetComponent<AudioSource>().Play();
            gameManager.gameOver();
        }
    }
}
